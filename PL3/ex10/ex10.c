#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "createshm.h"
#include "unlinkshm.h"

typedef struct {
	int buffer[10];
	int new_data;
}Shared_data;

int main(void){
	
	int size = sizeof(Shared_data), i, status, values = 0;
	Shared_data *shared_data;
	pid_t p;
	
	shared_data = (Shared_data *) create_shm(size);
	
	shared_data -> new_data = 0;
	
	p = fork();
	
	if (p == -1){
		perror("Fork falhou\n");
		exit(-1);
	} else if (p == 0){ //consumer
		while(values < 3){
			while(!shared_data -> new_data);
			for (i = 0; i < 10; i++){
				printf("%d\n", shared_data -> buffer[i]);
			}
			values++;
			shared_data -> new_data = 0;
		}		
	} else{ //producer
		while(values < 30){
			while(shared_data -> new_data);
			for (i = 0; i < 10; i++){
				shared_data -> buffer[i] = values++;
			}
			shared_data -> new_data = 1;
		}
	}
	
	wait(&status);
	
	unlinkshm(shared_data, size);
	
	return 0;
}


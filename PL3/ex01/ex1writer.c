#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

typedef struct {
		int number;
		char name[100];
	} sh_dt_tp;
	
int main(){

	int fd, data_size = sizeof(sh_dt_tp);
	sh_dt_tp * shared_data;
	fd = shm_open("/shmESTUDANTE",O_CREAT|O_EXCL|O_RDWR,S_IRUSR|S_IWUSR);
	
	if (fd == -1){
		perror("Erro na memória.\n");
		return 0;
	}
	
	ftruncate(fd,data_size);
	shared_data = (sh_dt_tp*)	mmap(NULL, 
										data_size,
										PROT_READ|PROT_WRITE,
										MAP_SHARED,
										fd,
										0);
	int nb;
	char nm[100];
	printf("Enter the number:");

	scanf("%d", &nb);
	printf("Enter the name:");
	
	scanf("%s", nm);
	shared_data->number = nb;
	//shared_data->name = nm;
	snprintf(shared_data->name, sizeof(shared_data->name), "%s", nm);
	
	
	
	return 0;
}

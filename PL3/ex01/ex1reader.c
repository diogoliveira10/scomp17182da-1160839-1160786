#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h> 
#include <fcntl.h> 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>


typedef struct {
		int number;
		char name[100];
	} sh_dt_tp;


int main(){
	int fd, data_size = sizeof(sh_dt_tp);
	sh_dt_tp * shared_data;
	fd = shm_open("/shmESTUDANTE",O_EXCL|O_RDWR,S_IRUSR|S_IWUSR);
	ftruncate (fd, data_size);
	shared_data = (sh_dt_tp *)	mmap(NULL, 
										data_size,
										PROT_READ|PROT_WRITE,
										MAP_SHARED,
										fd,
										0);
	printf("Numero do Estudante: %d\n", shared_data->number);
	printf("Nome do Estudante: %s\n", shared_data->name);
	close(fd);
	shm_unlink("/shmESTUDANTE");
	return 0;
}

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "createshm.h"
#include "unlinkshm.h"

typedef struct{
	int occurences[10];
	char* word[10];
	char path[10][100];
} Shared_data;

int main(void){
	
	int size = sizeof(Shared_data), i, status;
	Shared_data *shared_data;
	char line[1024];
	FILE *f;
	pid_t p;
	
	shared_data = (Shared_data *) create_shm(size);
	
	shared_data -> word[0] = "in";
	shared_data -> word[1] = "on";
	shared_data -> word[2] = "am";
	shared_data -> word[3] = "despite";
	shared_data -> word[4] = "hello";
	shared_data -> word[5] = "word";
	shared_data -> word[6] = "car";
	shared_data -> word[7] = "shy";
	shared_data -> word[8] = "a";
	shared_data -> word[9] = "an";
	
	for (i = 0; i < 10; i++){
		sprintf(shared_data -> path[i], "child%d.txt", i);
		p = fork();
		if (p == -1){
			perror("Fork falhou\n");
			exit(-1);
		} else if (p == 0){
			f = fopen(shared_data -> path[i], "r");
			while (fgets(line, sizeof(line), f) != NULL){
				if (strstr(line , shared_data -> word[i])!= NULL){
					shared_data -> occurences[i]++;
				}
			}
			fclose(f);
			exit(0);  
		}
	}
	
	while(wait(&status) > 0);
	
	for (i = 0; i < 10; i++){
		printf("Child %d found the word '%s' %d times in the file %s\n", i, shared_data -> word[i], shared_data -> occurences[i], shared_data -> path[i]);
	}
	
	unlinkshm(shared_data, size);
	
	return 0;
}


#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

typedef struct {
		int number;
	} sh_dt_tp;
	
int main(){
	int fd, data_size = sizeof(sh_dt_tp);
	int i;
	sh_dt_tp * shared_data;
	fd = shm_open("/shmNUMERO",O_CREAT|O_EXCL|O_RDWR,S_IRUSR|S_IWUSR);
	
	if (fd == -1){
		perror("Erro na memória.\n");
		return 0;
	}
	
	ftruncate(fd,data_size);
	shared_data= (sh_dt_tp*) mmap(NULL, data_size, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);
	
	shared_data->number=100;
	
	pid_t p = fork();
	
	if(p==-1){
		perror("Erro no fork!\n");
		return 0;
	}
	
	for(i=0;i<1000;i++){
		shared_data->number++;
		shared_data->number--;
		//printf("%d\n", shared_data->number);
	}
	printf("%d\n",shared_data->number);
	
	if(p>0){
		wait(NULL);
	}else{
		exit(0);
	}
	
	 
	return 0;
}

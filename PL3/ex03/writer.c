#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "createshm.h"

typedef struct{
	int arr[10];
} Shared_data;

int main(void){
	
	int size = sizeof(Shared_data), i;
	Shared_data *shared_data;
	time_t t;
	
	shared_data = (Shared_data *) create_shm(size);
	
	srand ((unsigned) time (&t));
	
	for (i = 0; i < 10; i++){
		shared_data -> arr[i] = rand() % (20 + 1 - 1) + 1;
	}
	
	munmap(shared_data, size);
	
	return 0;
}


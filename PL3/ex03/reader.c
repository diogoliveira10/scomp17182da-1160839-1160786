#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "unlinkshm.h"
#include "execshm.h"

typedef struct{
	int arr[10];
} Shared_data;

int main(void){
	
	int size = sizeof(Shared_data), i, sum = 0;
	Shared_data *shared_data;
	double avg;
	
	shared_data = (Shared_data *) exec_shm(size);
	
	for (i = 0; i < 10; i++){
		sum += shared_data -> arr[i];
	}
	
	avg = sum/10;
	
	printf("Average: %f\n", avg);
	
	unlinkshm(shared_data, size);
	
	return 0;
}

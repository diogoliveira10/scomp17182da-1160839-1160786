#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <time.h>
#include <sys/wait.h>

typedef struct {
		int area[10];
} sh_dt_tp;
	
int main() {
	int array[1000];
	srand((unsigned) time(NULL));
	int i;
	for(i=0; i<1000; i++){
		array[i] = rand() % 1000;
	}
	
	int fd, data_size = sizeof(sh_dt_tp);
	sh_dt_tp * shared_data;
	fd = shm_open("/shmARRAY", O_CREAT|O_EXCL|O_RDWR, S_IRUSR|S_IWUSR);
	
	if (fd == -1){
		perror("Erro na memória.\n");
		return 0;
	}
	
	ftruncate(fd, data_size);
	shared_data= (sh_dt_tp*) mmap(NULL, data_size, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);
	
	int j;
	
	for(i=0; i<10; i++) {
		pid_t p = fork();
		if (p == -1){
			perror("Erro no fork.\n");
			return 0;
		}
		if (p == 0){
			int max, aux;
			aux = i*100;
			max = 0;
			for (j = aux; j < aux+100 ; j++){
				if (array[j]>max){
					max = array[j];
				}				
			}
			shared_data->area[i]=max;
			exit(0);
		}
	}
	
	for (i = 0; i < 10; i++){
		wait(NULL);
	}
	
	int maxTotal = 0;
	for (i = 0; i < 10; i++){
		if ((shared_data->area[i]) > maxTotal){
			maxTotal = shared_data->area[i];
		}
	}
	
	printf("Nº máximo global: %d\n", maxTotal);
	close(fd);
	shm_unlink("/shmARRAY");
	return 0;
}

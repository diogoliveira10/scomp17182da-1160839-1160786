#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>

int main(void){
	
	int fd[3][2], buffer[10], i, j, status, values = 0;
	pid_t p;
	
	for(i = 0; i < 3; i++){
		if (pipe(fd[i]) == -1){
			perror("Pipe falhou\n");
			return -1;
		}
	}
	
	p = fork();
	
	if (p == -1){
		perror("Fork falhou\n");
		exit(-1);
	} else if (p == 0){ //consumer
		for (j = 0; j < 3; j++){
			close(fd[j][1]);
			read(fd[j][0], buffer, sizeof(buffer));
			close(fd[j][0]);
			for (i = 0; i < 10; i++){
				printf("%d\n", buffer[i]);
			}
		}	
	} else{ //producer
		for (j = 0; j < 3; j++){
			for (i = 0; i < 10; i++){
				buffer[i] = values++;
			}
			close(fd[j][0]);
			write(fd[j][1], buffer, sizeof(buffer));
			close(fd[j][1]);
		}
	}
	
	wait(&status);
		
	return 0;
}



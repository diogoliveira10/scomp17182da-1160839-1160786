#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>

int main(void){
	
	int fd[2], i, j, status, array[1000], max = 0, local = 0;
	pid_t p;
	time_t t;
	
	srand ((unsigned) time (&t));
	
	for (i = 0; i < 1000; i++){
		array[i] = rand() % (1000 + 1 - 0) + 0;
	}
	
	if (pipe(fd) == -1){
		perror("Pipe falhou\n");
		return -1;
	}
	
	for (i = 0; i < 10; i++){
		p = fork();
		if (p == -1){
			perror("Fork falhou\n");
			exit(-1);
		} else if (p == 0){
			for (j = 100*i; j < 100*(i+1); j++){
				if (max < array[j]) max = array[j];
			}
			
			printf("Child: %d Maximum: %d\n", i, max);
			
			close(fd[0]);
			write(fd[1], &max, sizeof(max));
			close(fd[1]);
			
			exit(0);
		}
	}
	
	close(fd[1]);
	while (read(fd[0], &local, sizeof(local)) > 0){
		if (max < local) max = local;
	}
	close(fd[0]);
	
	while(wait(&status) > 0);
	
	printf("Global maximum: %d\n", max);
	
	return 0;
}


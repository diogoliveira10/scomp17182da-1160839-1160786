#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>

void unlinkshm(void *shared_data, int size){
	munmap(shared_data, size);
	
	shm_unlink("/sharedm");
}

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "createshm.h"
#include "unlinkshm.h"

#define ARRAY_SIZE 1000000

typedef struct{
	int arr[ARRAY_SIZE];
} Shared_data;

int main(void){
	
	int size = sizeof(Shared_data), i, pipes[2], status, copy[ARRAY_SIZE];
	Shared_data *shared_data;
	Shared_data pipe_data;
	pid_t p;
	time_t start, end;
	
	shared_data = (Shared_data *) create_shm(size);
	
	if (pipe(pipes) == -1){
		perror("Pipe falhou\n");
		return -1;
	}
	
	p = fork();
	
	if (p == -1){
		perror("Fork falhou\n");
		exit(-1);
	} else if (p == 0){
		for (i = 0; i < ARRAY_SIZE; i++){
			 copy[i] = shared_data -> arr[i];
		}
		exit(0);
	} else{
		start = clock();
		for (i = 0; i < ARRAY_SIZE; i++){
			shared_data -> arr[i] = rand() % (20 + 1 - 1) + 1;
		}
		wait(&status);
		end = clock();
	}
	
	printf("Shared memory timer: %ld\n", ((end-start)));
	
	p = fork();
	
	if (p == -1){
		perror("Fork falhou\n");
		exit(-1);
	} else if (p == 0){
		close(pipes[1]);
		start = clock();
		for (i = 0; i < ARRAY_SIZE; i++){
			 read(pipes[0], &copy[i], sizeof(copy[i])); 
		}
		end = clock();
		close(pipes[0]);
		exit((end-start));
	} else{
		close(pipes[0]);
		for (i = 0; i < ARRAY_SIZE; i++){
			pipe_data.arr[i] = rand() % (20 + 1 - 1) + 1;
		}
		for (i = 0; i < ARRAY_SIZE; i++){
			write(pipes[1], &pipe_data.arr[i], sizeof(pipe_data.arr[i]));
		}
	}
	
	wait(&status);
	
	if(WIFEXITED(status)){
		printf("Pipe timer: %d\n", WEXITSTATUS(status));
	}
	
	unlinkshm(shared_data, size);
	
	return 0;
}

#define STR_SIZE 50
#define NR_DISC 10
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <time.h>
#include <sys/wait.h>

typedef struct{
	int numero;
	char nome[STR_SIZE];
	int disciplinas[NR_DISC];
}aluno;

int main() {
	
	int fd, data_size = sizeof(aluno);
	aluno * shared_data;
	fd = shm_open("/shmALUNO", O_CREAT|O_EXCL|O_RDWR, S_IRUSR|S_IWUSR);
	
	if(fd == -1){
		fd = shm_open("/shmALUNO", O_RDWR, S_IRUSR|S_IWUSR);
	}
	
	ftruncate(fd, data_size);
	shared_data= (aluno*) mmap(NULL, data_size, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);
	int total = NR_DISC-1;
	shared_data->disciplinas[total] = 0;
	pid_t p = fork();
	int i;
	if (p == -1) {
		perror("Erro ao criar filho.\n");
		return 0;
	} else if (p > 0){
		printf("Insira o número do Aluno:\n");
		int num;
		scanf("%d", &num);
		shared_data->numero = num;
		printf("Insira o nome do Aluno (APENAS UM):\n"); //só dá com um fgets é que dá com
		scanf("%s", shared_data->nome);
		
		printf("Insira as notas do Aluno:\n");
		for (i = 0; i < NR_DISC; i++){
			int not;
			scanf("%d", &not);
			shared_data->disciplinas[i] = not;
		}
		
		wait(NULL);		
	} else if (p==0) {
		while(shared_data->disciplinas[NR_DISC-1]==0);
		int h, l, aux=0, avg=0;
		h = shared_data->disciplinas[0];
		l = shared_data->disciplinas[0];
		for (i = 0; i < NR_DISC ; i++){
			aux = shared_data->disciplinas[i];
			if ( aux > h){
				h = aux;
			}
			if (aux < l) {
				l = aux;
			}
			avg = avg+aux;
		}
		avg = avg/NR_DISC;
		printf("----------------------------------------------------------\n");
		printf("Nome: %s | Número mecanográfico: %d\n",  shared_data->nome,shared_data->numero);
		printf("Highest Grade = %d\n", h);
		printf("Average Grade = %d\n", avg);
		printf("Lowest Grade = %d\n", l);
		exit(0);
	}
	
	munmap(shared_data,data_size);
	close(fd);
	shm_unlink("/shmALUNO");
	
	return 0;
}


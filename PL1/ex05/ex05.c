#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>

int main(void){
	
	pid_t p;
	int i, status;
	
	for (i = 0; i < 2; i++){
		
		p = fork();
		if (p == -1){
			
			perror("Fork falhou\n");
			exit(-1);
		} else if (p == 0){
			
			sleep(i+1);
			exit(i+1);
		}
	}
	
	while (wait(&status) > 0){
		
		if (WIFEXITED(status)){
			printf("O filho retornou %d\n", WEXITSTATUS(status));
		}
	}
	
	return 0;
}


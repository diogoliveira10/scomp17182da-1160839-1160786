#define ARRAY_SIZE 2000

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>

int main(void){
	
	int numbers[ARRAY_SIZE], i, n, j, ind, status;
	
	time_t t;
	pid_t p1;
	
	srand ((unsigned) time (&t));
	
	for (i = 0; i < ARRAY_SIZE; i++){
		
		numbers[i] = rand() % 1000;
	}
	
	n = rand() % 1000;
	
	for (i = 0; i < 10; i++){
		
		p1 = fork();
		if (p1 == -1){
			
			perror("Fork falhou\n");
			exit(-1);
			
		} else if (p1 == 0){
			
			ind = 0;
			for (j = i*200; j < (ARRAY_SIZE/10)*(i+1); j++){
				
				if (numbers[j] == n) exit(j);
				ind++;
			}
			exit(255);
		}
	}
	
	while (wait(&status) > 0){
		
		if (WIFEXITED(status)){
			
			if (WEXITSTATUS(status) != 255){
				
				printf("Index: %d\n", WEXITSTATUS(status));
			} else {
				
				printf("O filho nao encontrou o elemento\n");
			}
		} else {
			printf("O filho nao terminou corretamente\n");
		}
	}
	
	return 0;
}


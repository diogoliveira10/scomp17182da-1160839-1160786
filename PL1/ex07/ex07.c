#define ARRAY_SIZE 1000

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>

int main(void){
	
	int numbers[ARRAY_SIZE], n, i, freq = 0, status;
	time_t t;
	
	srand ((unsigned) time (&t));
	
	for (i = 0; i < ARRAY_SIZE; i++){
		
		numbers[i] = rand() % 1000;
	}
	
	n = rand() % 1000;
	
	pid_t p1;
	
	p1 = fork();
	
	if (p1 == -1){
		
		perror("Failed fork\n");
		exit(-1);
	}
	
	if (p1 == 0){
		
		for (i = 0; i < (ARRAY_SIZE/2); i++){
			
			if (numbers[i] == n){
				freq++;
			}
		}
		exit(freq);
		
	} else {
		for (i = ARRAY_SIZE/2; i < ARRAY_SIZE; i++){
			
			if (numbers[i] == n){ 
				freq++;
			}
		}
	}
	
	wait(&status);
	
	if (WIFEXITED(status)){
		printf("Child half: %d\n", WEXITSTATUS(status));
	} else {
		
		printf("Filho nao terminou normalmente\n");
	}
	
	printf("Parent half: %d\n", freq);
	printf("The number %d was found %d times in the array\n", n, freq+WEXITSTATUS(status));
	
	return 0;
}



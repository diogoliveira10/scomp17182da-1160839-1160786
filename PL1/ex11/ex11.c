#define ARRAY_SIZE 1000

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>

int main(void){
	
	int numbers[ARRAY_SIZE], result[ARRAY_SIZE], i, j, child_max = 0, max = 0, status;
	
	time_t t;
	pid_t p1;
	
	srand ((unsigned) time (&t));
	
	for (i = 0; i < ARRAY_SIZE; i++){
		
		numbers[i] = rand() % (255 + 1 - 0) + 0;
	}
	
	for (i = 0; i < 5; i++){
		
		p1 = fork();
		if (p1 == -1){
			
			perror("Fork falhou\n");
			exit(-1);
		} else if(p1 == 0){
			
			j = (ARRAY_SIZE/5)*i;
			
			while(j < (ARRAY_SIZE/5)*(i+1)){
				
				if (numbers[j] >= child_max) child_max = numbers[j];
				
				j++;
			}
			
			exit(child_max);
		}
	}
	
	while (wait(&status) > 0){
		
		if (WIFEXITED(status)){
			
			if (WEXITSTATUS(status) > max) max = WEXITSTATUS(status);
			
		} else {
			printf("O filho nao terminou corretamente\n");
		}
	}
	
	printf("Max: %d\n", max);
	
	p1 = fork();
	
	if (p1 == -1){
		
		perror("Fork falhou\n");
		exit(-1);
		
	} else if (p1 == 0){
		
		for (i = 0; i < ARRAY_SIZE/2; i++){
			
			result[i] = (numbers[i]*100)/max;
			printf("Index: %d Result: %d\n", i, result[i]);
		}
		
		exit (0);
	} else{
		wait(&status);
		
		for (i = ARRAY_SIZE/2; i < ARRAY_SIZE; i++){
			
			result[i] = (numbers[i]*100)/max;
			
			printf("Index: %d Result: %d\n", i, result[i]);
		}
	}
	
	return 0;
}


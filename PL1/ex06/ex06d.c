#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>

int main(void){
	
	int i, status;
	pid_t p1;
	
	for (i = 0; i < 4; i++){
		p1 = fork();
		
		if (p1 == -1){
			perror("Failed fork\n");
			exit(-1);
			
		} else if (p1 == 0){
			
			sleep(1);
			printf("Creation order: %d\n", i + 1);
			exit(i + 1);
		}
	}
	
	while(wait(&status) > 0); //test
	
	printf("This is the end.\n");
			
	
	return 0;
}


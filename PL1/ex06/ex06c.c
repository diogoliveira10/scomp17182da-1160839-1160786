#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>

int main(void){
	
	int i, j = 0, status;
	pid_t p1, child_pids[4];
	
	for (i = 0; i < 4; i++){
		p1 = fork();
		
		if (p1 == -1){
			perror("Failed fork\n");
			
			exit(-1);
		} else if (p1 == 0){
			
			if ((getpid() % 2) != 0){ //test
				
				printf("Even.\n");
			}
			sleep(1);
			exit(1);
		} else {
		
			if (p1 % 2 != 0){
				child_pids[j] = p1;
				j++;
			}
		}
	}
	
	for (i = 0; i <= j; i++){
		
		waitpid(child_pids[i], &status, 0);
	}
	
	printf("This is the end.\n");
			
	
	return 0;
}


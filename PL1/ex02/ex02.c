#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>

int main(void){
	
	pid_t p1, p2, p3;
	int status;
	
	printf("I'm..\n");
	
	p1 = fork();
	
	if (p1 == -1){
		
		perror("Failed fork\n");
		exit(-1);
	}
	if (p1 == 0){
		
		printf("I'll never join you!\n");
		exit(0);
	} else if(p1 > 0){
		
		wait(&status);
		printf("the..\n");
		p2 = fork();
		if (p2 == 0){
		
			printf("I'll never join you!\n");
			exit(0);
		} else if (p2 > 0){
		
			wait(&status);
			printf("father!\n");
		
			p3 = fork();
			if (p3 == 0){
		
				printf("I'll never join you!\n");
				exit(0);
			}
		}
	}
	
	wait(&status);
	
	return 0;
}


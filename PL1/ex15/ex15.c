#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

int main(void){
	
	int status;
	
	char fich[100];
	
	pid_t p1;
	
	printf("Introduza o nome do ficheiro ('sair' para terminar)\n");
	scanf("%s", fich);
	
	while (strcmp("sair", fich) != 0){
		
		p1 = fork();
		if (p1 == -1){
			
			perror("Fork falhou\n");
			exit(-1);
		}	
		if (p1 == 0){
			
			execlp("ls", "ls", fich, NULL);
			exit(-1);
		} else{
			
			wait(&status);
			
			if (WIFEXITED(status)){
				
				printf("%d\n", WEXITSTATUS(status));
				
				if (WEXITSTATUS(status) == 2){
					
					printf("O ficheiro %s nao existe\n", fich);
				} else if (WEXITSTATUS(status) == 0) {
					
					printf("O ficheiro %s existe\n", fich);
				} else {
					printf("Erro de execucao\n");
				}
			}
		}
		
		printf("Introduza o nome do ficheiro ('sair' para terminar)\n");
		scanf("%s", fich);
	}
	
	return 0;
}


#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>
#include "ex12.h"

int main(void){
	
	int n = 6, status;
	
	if(spawn_childs(n) != 0) exit(0);
	
	while (wait(&status) > 0){
		if (!WIFEXITED(status)) printf("Filho nao terminou corretamente.\n");
	}
	
	printf("father\n");
	
	
	return 0;
}


#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>

int spawn_childs(int n){
	
	int i;
	pid_t p1;
	
	for (i = 0; i < n; i++){
		
		p1 = fork();
		
		if (p1 == -1){
			
			perror("Failed fork\n");
			exit(-1);
			
		} else if (p1 == 0){
			
			printf("child\n");
			return((i + 1) * 2);
		}
	}
	
	return 0;
}


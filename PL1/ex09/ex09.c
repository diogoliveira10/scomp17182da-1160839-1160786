#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>

int main(void){
	
	int i, j, status = 1;
	pid_t p1;
	
	for (i = 0; i < 10; i++){
		p1 = fork();
		
		if(p1 == 0){
			for (j = i * 100; j < (i + 1) * 100; j++){
				
				printf("Child %d: %d\n", i + 1, j);
			}
			exit(j);
			
		} else if(p1 == -1){
			
			printf("Failed fork\n");
			exit(-1);
		}
	}
	
	while (wait(&status) > 0){
		
		if (!WIFEXITED(status)) printf("O filho nao terminou corretamente.\n");
	}
	
	return 0;
}


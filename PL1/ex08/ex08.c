#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>

int main(void){
	
	pid_t p1;
	
	if (fork() == 0){
		
		printf("PID = %d\n", getpid()); exit(0);
	}
	
	if ((p1=fork()) == 0){
		
		printf("PID = %d\n", getpid()); exit(0);
	}
	
	printf("Parent PID = %d\n", getpid());
	
	printf("Waiting... (for PID=%d)\n", p1);
	
	waitpid(p1, NULL, 0);
	
	printf("Enter Loop...\n");
	
	while(1);
	
	return 0;
}


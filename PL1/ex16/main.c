#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include "ex16.h"

int main(void){
	
	int ret;
	
	ret = my_exec("ls");
	
	if (ret == 0){ 
		
		printf("Command %s ran without errors\n", "ls");
	} else {
		
		printf("Command %s error\n", "ls");
	}
	
	ret = my_exec("ps");
	
	if (ret == 0){ 
		printf("Command %s ran without errors\n", "ps");
	} else {
		printf("Command %s error\n", "ps");
	}
	
	ret = my_exec("who");
	
	
	if (ret == 0){ 
		
		printf("Command %s ran without errors\n", "who");
	} else {
		
		printf("Command %s error\n", "who");
	}
	
	return 0;
}


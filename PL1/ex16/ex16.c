#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

int my_exec(char* command){
	
	int status;
	pid_t p1;
	
	p1 = fork();
	
	if (p1 == -1){
		
		perror("Fork falhou\n");
		exit(-1);
		
	} else if (p1 == 0){
		
		execlp(command, command, NULL);
		exit(-1);
		
	} else {
		
		wait(&status);
		
		if (WIFEXITED(status)){
			
			return WEXITSTATUS(status);
		}
	}
	return -1;
}	

#define ARRAY_SIZE 1000

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

int main(void){
	
	int fd[2], vec1[1000], vec2[1000], i, j, tmp = 0, res = 0;
	pid_t p;
	time_t t;
	
	srand ((unsigned) time (&t));
	
	for (i = 0; i < ARRAY_SIZE; i++){
		vec1[i] = rand() % 1000;
		vec2[i] = rand() % 1000;
	}
	
	if (pipe(fd) == -1){
		perror("Pipe falhou\n");
		return -1;
	}
	
	for (i = 0; i < 5; i++){
		p = fork();
		if (p == -1){
			perror("Fork falhou\n");
			exit(-1);
		} else if (p == 0){
			close(fd[0]);
			for (j = (ARRAY_SIZE/5)*i; j < (ARRAY_SIZE/5)*(i+1); j++){
				tmp += vec1[j]+vec2[j];
			}
			write(fd[1], &tmp, sizeof(tmp));
			close(fd[1]);
			exit(0);
		}
	}
	
	close(fd[1]);
	
	while(read(fd[0], &tmp, sizeof(tmp)) > 0){
		res += tmp;
	}
	
	close(fd[0]);
	
	printf("Final result %d\n", res);
		
	return 0;
}


#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

int main(void){
	
	int fd[4], status, i, nBytes;
	pid_t p;
	char msg[256];
	
	for (i = 0; i < 2; i++){
		if (pipe(fd+i*2) == -1){
			perror("Pipe falhou\n");
			return -1;
		}
	}
	
	p = fork();
	
	if (p == -1){
		perror("Fork falhou\n");
		exit(-1);
	} else if (p == 0){
		close(fd[0]);
		close(fd[3]);
		printf("Introduza a mensagem:\n");
		scanf("%s", msg);
		write(fd[1], msg, strlen(msg)+1);
		read(fd[2], msg, 256);
		printf("Message: %s\n", msg);
		close(fd[1]);
		close(fd[2]);
		exit(0);
	} else {
		close(fd[1]);
		close(fd[2]);
		nBytes = read(fd[0], msg, 256);
		for (i = 0; i < nBytes; i++){
			if (msg[i] >= 'a' && msg[i] <= 'z'){
				msg[i] -= 32;
			} else {
				msg[i] += 32;
			}
		}
		write(fd[3], msg, strlen(msg)+1);
		wait(&status);
		close(fd[0]);
		close(fd[3]);
	}
	
	return 0;
}


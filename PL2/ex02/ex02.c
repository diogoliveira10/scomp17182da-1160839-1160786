#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

int main(void){
	
	int fd[2], x, status;
	char str[100];
	pid_t p;
	
	if (pipe(fd) == -1){
		perror("Pipe falhou\n");
		return -1;
	}
	
	p = fork();
	
	if (p == -1){ //erro a criar o filho
		perror("Fork falhou\n");
		exit(-1);
	} else if (p > 0){ //pai
		//fecha a extremidade não usada (leitura)
		close(fd[0]);
		
		printf("Introduza um inteiro:\n");
		scanf("%d", &x);
		printf("Introduza uma string:\n");
		scanf("%s", str);
		
		//escreve a informação no pipe
		write(fd[1], &x, sizeof(x));
		write(fd[1], str, strlen(str)+1);
		
		//fecha a extremidade de escrita
		close(fd[1]);
		
		wait(&status);
	} else { //filho
		//fecha a extremidade não usada (escrita)
		close(fd[1]);
		
		//lê a informação do pipe e faz print dessa informação
		read(fd[0], &x, sizeof(x));
		printf("O filho leu o inteiro %d\n", x);
		read(fd[0], str, 100);
		printf("O filho leu a string %s\n", str);
		
		//fecha a extremidade de leitura
		close(fd[0]);
		
		exit(0);
	}
		
	return 0;
}


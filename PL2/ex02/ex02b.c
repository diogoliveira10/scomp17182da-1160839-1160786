#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

typedef struct{
	int x;
	char str[100];
}Info;

int main(void){
	
	Info i; //maneira de armazenar várias informações e fazer uma só operação
	int fd[2], status;
	pid_t p;
	
	if (pipe(fd) == -1){
		perror("Pipe falhou\n");
		return -1;
	}
	
	p = fork();
	
	if (p == -1){
		perror("Fork falhou\n");
		exit(-1);
	} else if (p > 0){
		close(fd[0]);
		printf("Introduza um inteiro:\n");
		scanf("%d", &i.x);
		printf("Introduza uma string:\n");
		scanf("%s", i.str);
		write(fd[1], &i, sizeof(i));
		close(fd[1]);
		wait(&status);
	} else {
		close(fd[1]);
		read(fd[0], &i, sizeof(i));
		printf("O filho leu o inteiro %d\n", i.x);
		printf("O filho leu a string %s\n", i.str);
		close(fd[0]);
		exit(0);
	}
		
	return 0;
}


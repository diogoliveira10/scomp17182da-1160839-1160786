#define PROCESSES 6

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

int main(void){
	
	int i, fd[PROCESSES][2], gen, received, status;
	pid_t p;
	time_t t;
	
	for (i = 0; i < PROCESSES; i++){
		if (pipe(fd[i]) == -1){
			perror("Pipe failure\n");
			return -1;
		}
	}
	
	
	for (i = 0; i < PROCESSES-1; i++){
		p = fork();
		if (p == -1){
			perror("Fork failure\n");
			exit(-1);
		} else if (p == 0){
			close(fd[i][1]);
			close(fd[i+1][0]);
			
			srand ((unsigned) time (&t) - getpid());
			
			gen = rand() % (500 + 1 - 1) + 1;
			
			printf("PID: %d Number: %d\n", getpid(), gen);
			read(fd[i][0], &received, sizeof(received));
			
			if (received > gen) {
				write(fd[i+1][1], &received, sizeof(received));
			} else {
				write(fd[i+1][1], &gen, sizeof(gen));
			}
			
			close(fd[i+1][1]);
			close(fd[i][0]);
			
			exit(0);  
		}
	}
	
	srand ((unsigned) time (&t) - getpid());
	gen = rand() % (500 + 1 - 1) + 1;
	
	printf("PID: %d Number: %d\n", getpid(), gen);
	
	close(fd[0][0]);
	
	write(fd[0][1], &gen, sizeof(gen));
	
	close(fd[0][1]);
	
	close(fd[PROCESSES-1][1]);
	read(fd[PROCESSES-1][0], &received, sizeof(received));
	
	printf("Greatest number: %d\n", received);
	
	close(fd[PROCESSES-1][0]);
	
	while(wait(&status) > 0);
	
	return 0;
}


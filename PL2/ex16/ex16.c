#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#define EXECS 3

int main(void){
	
	int fd[EXECS-1][2], i, status;
	pid_t p;
	
	for (i = 0; i < EXECS-1; i++){
		if (pipe(fd[i]) == -1){
			perror("Pipe falhou\n");
			return -1;
		}
	}
	
	for (i = 0; i < EXECS; i++){
		p = fork();
		if (p == -1){
		perror("Fork falhou\n");
		exit(-1);
		} else if (p == 0){
			if (i == 0){
				close(fd[i][0]);
				dup2(fd[i][1], 1);
				
				close(fd[i][1]);
				execlp("ls", "ls", "-la", NULL);
			} else if (i == 1){
				close(fd[i-1][1]);
				close(fd[i][0]);
				
				dup2(fd[i-1][0], 0);
				dup2(fd[i][1], 1);
				
				close(fd[i-1][0]);
				close(fd[i][1]);
				
				execlp("sort", "sort", NULL);
			} else if (i == 2){
				close(fd[i-1][1]);
				dup2(fd[i-1][0], 0);
				
				close(fd[i-1][0]);
				execlp("wc", "wc", "-l", NULL);
			}
			perror("Exec falhou\n");
			exit(-1);
		}
	}
	wait(&status);

	return 0;
}

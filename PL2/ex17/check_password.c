#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#define MATCH 0
#define INVALID_PASSWORD 1
#define NO_USER 2

int main(void){
	
	int id, fd[2], status;
	char password[100];
	pid_t p;
	
	printf("Introduza o id: \n");
	scanf("%d", &id);
	
	printf("Introduza a password: \n");
	scanf("%s", password);
	
	if (pipe(fd) == -1){
		perror("Pipe falhou\n");
		return -1;
	}
	
	p = fork();
	
	if (p == 0){
		write(fd[1], &id, sizeof(id));
		write(fd[1], password, strlen(password)+1);
		close(fd[1]);
		dup2(fd[0], 0);
		close(fd[0]);
		
		execlp("./validate", "./validate", NULL);
		perror("Exec falhou\n");
		exit(-1);
	}
	
	wait(&status);
	
	if (WIFEXITED(status)){
		if (WEXITSTATUS(status) == MATCH){
			printf("Password verified\n");
		} else if (WEXITSTATUS(status) == INVALID_PASSWORD){
			printf("Invalid password\n");
		} else {
			printf("No such user\n");
		}
	}
	
	return 0;
}


#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#define MATCH 0
#define INVALID_PASSWORD 1
#define NO_USER 2

typedef struct{
	int id;
	char password[100];
} User;

int main(void){
	
	User data, user;
	
	data.id = 5; 
	
	char password[100] = "pass"; 
	
	strcpy(data.password, password);
	
	scanf("%d", &user.id);
	scanf("%s", user.password);
	
	if (data.id == user.id && strcmp(data.password, user.password) == 0){
		exit(MATCH);
	} else if (data.id == user.id && strcmp(data.password, user.password) != 0){
		exit(INVALID_PASSWORD);
	}
	
	exit(NO_USER);
	
	return 0;
}


#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>

int main(void){
	
	int fd[2], pid, status;
	pid_t p;
	
	if (pipe(fd) == -1){
		perror("Pipe falhou\n");
		return -1;
	}
	
	p = fork();
	
	if (p == -1){ //erro a criar filho
		perror("Fork falhou\n");
		exit(-1);
	} else if (p > 0){ //pai
		//fecha a extremidade não usada (leitura)
		close(fd[0]);
		
		pid = getpid();
		printf("PID do pai: %d\n", pid);
		//escreve a informação no pipe
		write(fd[1], &pid, sizeof(pid));
		
		//fecha a extremidade de escrita
		close(fd[1]);
		
		wait(&status);
	} else { //filho
		//fecha a extremidade não usada (escrita)
		close(fd[1]);
		
		//lê a informação do pipe
		read(fd[0], &pid, sizeof(pid));
		printf("PID lido pelo filho: %d\n", pid);
		
		//fecha a extremidade de leitura
		close(fd[0]);
		
		exit(0);
	}
	return 0;
}

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

int main(void){
	
	char l[100] = "Line ";
	int fd[2], status, i;
	pid_t p;
	
	if (pipe(fd) == -1){
		perror("Pipe falhou\n");
		return -1;
	}
	
	p = fork();
	
	if (p == -1){
		perror("Pipe falhou\n");
		return -1;
	} else if (p == 0){
		close(fd[1]);
		dup2(fd[0], 0);
		
		close(fd[0]);
		
		execlp("more", "more", NULL);
		
		perror("Exec falhou\n");
		exit(-1);
	} else if (p > 0){
		close(fd[0]);
		
		for(i = 1; i <= 100; i++){
			sprintf(l, "Line %d\n", i);
			write(fd[1], l, strlen(l));
		}
		
		close(fd[1]);
		wait(&status);
	}
	
	return 0;
}


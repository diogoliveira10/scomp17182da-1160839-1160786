#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

int main(void){
	
	int fd[4], status, n, balance = 20, bet, aut = 1, i;
	pid_t p;
	time_t t;
	
	srand ((unsigned) time (&t));
	
	for (i = 0; i < 2; i++){
		if (pipe(fd+i*2) == -1){
			perror("Pipe falhou\n");
			return -1;
		}
	}
	
	p = fork();
	
	if (p == -1){
		perror("Fork falhou\n");
		exit(-1);
	} else if (p > 0){
		close(fd[0]);
		close(fd[3]);
		
		while (balance > 0){
			n = rand() % (5 + 1 - 1) + 1;
			
			write(fd[1], &aut, sizeof(aut));
			read(fd[2], &bet, sizeof(bet));
			
			if(bet == n){
				balance += 10;
			} else {
				balance -= 5;
			}
			
			write(fd[1], &balance, sizeof(balance));
		}
		
		aut = 0;
		write(fd[1], &aut, sizeof(aut));
		
		close(fd[1]);
		close(fd[2]);
		
		wait(&status);
	} else {
		close(fd[1]);
		close(fd[2]);
		
		read(fd[0], &aut, sizeof(aut));
		printf("%d\n", aut);
		
		while (aut == 1){
			n = rand() % (5 + 1 - 1) + 1;
			
			write(fd[3], &n, sizeof(n));
			read(fd[0], &balance, sizeof(balance));
			
			printf("Credit: %d\n", balance);
			read(fd[0], &aut, sizeof(aut));
		}
		
		close(fd[0]);
		close(fd[3]);
		
		exit(0);
	}
			
	return 0;
}


#define CHILDS 10
#define ARRAY_SIZE 50000

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

typedef struct{
	int customer_code;
	int product_code;
	int quantity;
}Sales;

int main(void){
	
	int fd[2], products[ARRAY_SIZE], i, j;
	
	Sales s[ARRAY_SIZE];
	time_t t;
	pid_t p;
	
	srand ((unsigned) time (&t));
	
	for (i = 0; i < ARRAY_SIZE; i++){
		s[i].customer_code = rand() % 1000;
		s[i].product_code = rand() % 1000;
		
		s[i].quantity = rand() % (21 + 1 - 0) + 0;
	}
	
	if (pipe(fd) == -1){
		perror("Pipe failure\n");
		return -1;
	}
	
	for (i = 0; i < CHILDS; i++){
		p = fork();
		if (p == -1){
			perror("Fork failure\n");
			exit(-1);
		} else if (p == 0){
			close(fd[0]);
			
			for (j = (ARRAY_SIZE/CHILDS)*i; j < (ARRAY_SIZE/CHILDS)*(i+1); i++){
				if (s[j].quantity > 20){
					write(fd[1], &s[j].product_code, sizeof(s[j].product_code));
				}
			}
			
			close(fd[1]);
			exit(0);
		}
	}
	
	close(fd[1]);
	
	i = 0;
	
	while(read(fd[0], &j, sizeof(j)) > 0){
		products[i] = j;
		i++;
	}
	
	close(fd[0]);
	
	while (i >= 0){
		printf("Product %d\n", products[i]);
		i--;
	}
		
		
	return 0;
}


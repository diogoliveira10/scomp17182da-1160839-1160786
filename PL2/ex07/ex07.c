#define ARRAY_SIZE 1000
#define CHILDS 5

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

int main(void){
	
	int fd[CHILDS][2], vec1[ARRAY_SIZE], vec2[ARRAY_SIZE], i, j, tmp = 0;
	pid_t p;
	time_t t;
	
	srand ((unsigned) time (&t));
	
	for (i = 0; i < ARRAY_SIZE; i++){
		vec1[i] = rand() % 1000;
		vec2[i] = rand() % 1000;
	}
	
	for (i = 0; i < CHILDS; i++){
		if (pipe(fd[i]) == -1){
			perror("Pipe falhou\n");
			return -1;
		}
	}
	
	for (i = 0; i < CHILDS; i++){
		p = fork();
		if (p == -1){
			perror("Fork falhou\n");
			exit(-1);
		} else if (p == 0){
			close(fd[i][0]);
			for (j = (ARRAY_SIZE/CHILDS)*i; j < (ARRAY_SIZE/CHILDS)*(i+1); j++){	
				tmp += vec1[j]+vec2[j];
				write(fd[i][1], &tmp, sizeof(tmp));
			}
			close(fd[i][1]);
			exit(0);
		}
	}
	
	int result[ARRAY_SIZE];
	
	j = 0;
	
	for (i = 0; i < CHILDS; i++){
		close(fd[i][1]);
		while(read(fd[i][0], &tmp, sizeof(tmp)) > 0){
			result[j] = tmp;
			printf("Index: %d Result: %d\n", j, result[j]);
			j++;
		}
		close(fd[i][0]);
	}
	
	return 0;
}


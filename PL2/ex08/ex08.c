#define CHILDS 10
#define ROUNDS 10

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

typedef struct{
	int round;
	char* msg;
}Game;

int main(void){
	
	int fd[2], status, i, pids[CHILDS];
	pid_t p;
	Game g;
	
	if (pipe(fd) == -1){
		perror("Pipe failure\n");
		return -1;
	}
	
	for (i = 0; i < CHILDS; i++){
		p = fork();
		
		if (p == -1){
			perror("Fork failure\n");
			
			exit(-1);
		} else if (p > 0){
			pids[i] = p;
		} else {
			close(fd[1]);
			read(fd[0], &g, sizeof(g));
			
			printf("%s in round %d\n", g.msg, g.round);
			
			close(fd[0]);
			exit(g.round);
		}
	}
	
	g.msg = "Win";
	close(fd[0]);
	
	for (i = 0; i < ROUNDS; i++){
		sleep(2);
		g.round = i+1;
		
		write(fd[1], &g, sizeof(g));
	}
	
	close(fd[1]);
	
	i = 0; //reset the counter
	
	for (i = 0; i < CHILDS; i++){
		waitpid(pids[i], &status, 0);
		
		if (WIFEXITED(status)) 
			printf("Child %d won in round %d\n", pids[i], WEXITSTATUS(status));
	}
	
	return 0;
}


#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

int main(void){
	
	int fd[2], status;
	char res[100];
	pid_t p;
	
	if (pipe(fd) == -1){
		perror("Pipe falhou\n");
		return -1;
	}
	
	p = fork();
	
	if (p == -1){
		perror("Fork falhou\n");
		exit(-1);
	} else if (p > 0){
		close(fd[0]);
		char h[100] = "Hello world!\n";
		write(fd[1], h, strlen(h));
		char g[100] = "Goodbye!\n";
		write(fd[1], g, strlen(g)+1);
		close(fd[1]);
		wait(&status);
		if (WIFEXITED(status)){
			printf("O filho %d terminou com o valor %d\n", p, WEXITSTATUS(status));
		}
	} else {
		close(fd[1]);
		while (read(fd[0], res, 100) > 0){
			printf("%s", res);
		}
		close(fd[0]);
		exit(0);
	}
	
	return 0;
}


#define READERS 5
#define DATABASE 100

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

typedef struct{
	char* name;
	char* barcode;
	int price;
	int reader;
}Product;

int main(void){
	
	int fd[2], i, pipes[READERS][2], status;
	Product product;
	pid_t p;
	
	if (pipe(fd) == -1){
		perror("Pipe falhou\n");
		return -1;
	}
	
	for (i = 0; i < READERS; i++){
		if (pipe(pipes[i]) == -1){
			perror("Pipe falhou\n");
			return -1;
		}
	}
	
	for (i = 0; i < READERS; i++){
		p = fork();
		if (p == -1){
			perror("Fork falhou\n");
			exit(-1);
		} else if (p == 0){
			close(fd[0]);
			close(pipes[i][1]);
			scanf("%s", product.barcode);
			
			product.reader = i;
			
			write(fd[1], &product, sizeof(product));
			read(pipes[i][0], &product, sizeof(product));
			
			printf("Product: %s Price: %d\n", product.name, product.price);
			
			close(fd[1]);
			close(pipes[i][1]);
			
			exit(0);
		}
	}
	
	close(fd[1]);
	
	Product data[DATABASE];
	
	while(read(fd[0], &product, sizeof(product)) > 0){
		for(i = 0; i < DATABASE; i++){
			if (strcmp(data[i].barcode, product.barcode) == 0){
				write(pipes[product.reader][1], &data[i], sizeof(data[i]));
			}
		}
	}
	
	close(fd[0]);
	while(wait(&status) > 0);
	
	return 0;
}


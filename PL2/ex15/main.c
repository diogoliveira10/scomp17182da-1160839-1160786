#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

int main(void){
	
	int fd[4], status, number, factorial, i;
	pid_t p;
	
	for (i = 0; i < 2; i++){
		if (pipe(fd+i*2) == -1){
			perror("Pipe falhou\n");
			return -1;
		}
	}
	
	p = fork();
	
	if (p == -1){
		perror("Fork falhou\n");
		exit(-1);
	} else if (p > 0){
		close(fd[0]);
		
		printf("Introduza o numero\n");
		scanf("%d", &number);
		
		write(fd[1], &number, sizeof(number));
		close(fd[1]);
		close(fd[3]);
		
		read(fd[2], &factorial, sizeof(factorial));
		printf("Factorial: %d\n", factorial);
		
		close(fd[2]);
		wait(&status);
	} else{
		close(fd[1]);
		dup2(fd[0], 0);
		
		close(fd[0]);
		close(fd[2]);
		
		dup2(fd[3], 1);
		close(fd[3]);
		
		execlp("./factorial", "./factorial", NULL);
		perror("exec error");
		exit(-1);
	}
	
	return 0;
}


#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

int main(void){
	
	int number, i, factorial = 1;
	scanf("%d", &number);
	
	for (i = 2; i <= number; i++){
		factorial *= i;
	}
	
	printf("%d", 2);
	return 0;
}


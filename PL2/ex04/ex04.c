#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

int main(void){
	
	int fd[2], status;
	pid_t p;
	char res[100];
	
	if (pipe(fd) == -1){
		perror("Pipe falhou\n");
		return -1;
	}
	
	p = fork();
	
	if (p == -1){
		perror("Fork falhou\n");
		exit(-1);
	} else if (p > 0){
		close(fd[0]);
		FILE *f;
		char str[100];
		char* filename = "scomp.txt";
		f = fopen(filename, "r");
		if (f == NULL){
			printf("Ocorreu um erro a abrir o ficheiro %s\n", filename);
			return -1;
		}
		while (fgets(str, 100, f) != NULL){
			write(fd[1], str, strlen(str)+1);
		}
		fclose(f);
		close(fd[1]);
		wait(&status);
	} else {
		close(fd[1]);
		while (read(fd[0], res, 100) > 0){
			printf("%s\n", res);
		}
		close(fd[0]);
		exit(0);
	}
	
	return 0;
}


#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>


int main(void){
	
	int fd[2], status;
	char fich[500];
	pid_t p;
	
	if (pipe(fd) == -1){
		perror("Pipe falhou\n");
		return -1;
	}
	
	p = fork();
	
	if (p == -1){
		perror("Pipe falhou\n");
		return -1;
	} else if (p == 0){
		close(fd[0]);
		dup2(fd[1], 1);
		
		close(fd[1]);
		
		execlp("sort", "sort", "fx.txt", NULL);
		
		perror("Exec falhou\n");
		exit(-1);
	} else if (p > 0){
		close(fd[1]);
		
		read(fd[0], fich, 500);
		printf("%s\n", fich);
		
		close(fd[0]);
		wait(&status);
	}
	
	return 0;
}


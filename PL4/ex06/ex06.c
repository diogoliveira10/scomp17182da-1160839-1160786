#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <semaphore.h>
#include "createshm.h"
#include "unlinkshm.h"

typedef struct {
	int msg;
}Shared_data;

int main(void){

	sem_t *sem_father, *sem_child;
	pid_t p;
	int status, size = sizeof(Shared_data);
	Shared_data *shared_data;

	if ((sem_father = sem_open("pai", O_CREAT|O_EXCL, 0644, 0)) == SEM_FAILED) {
		perror("No sem_open()");
		exit(1);
	}

	if ((sem_child = sem_open("filho", O_CREAT|O_EXCL, 0644, 1)) == SEM_FAILED) {
		perror("No sem_open()");
		exit(1);
	}

	shared_data = (Shared_data *) create_shm(size);

	p = fork();

	if (p == -1){
		perror("Fork failed\n");
		exit (-1);
	} else if (p == 0){
		while (shared_data -> msg < 15){
			sem_wait(sem_child);
			printf("I'm the child\n");
			shared_data -> msg++;
			sem_post(sem_father);
		}
		exit(1);
	} else{
		while (shared_data -> msg < 14){
			sem_wait(sem_father);
			printf("I'm the father\n");
			shared_data -> msg++;
			sem_post(sem_child);
		}
	}

	wait(&status);

	sem_unlink("pai");

	sem_unlink("filho");

	unlinkshm(shared_data, size);

	return 0;
}

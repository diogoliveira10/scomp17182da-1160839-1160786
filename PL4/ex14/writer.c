#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <semaphore.h>

typedef struct{
	char string[100];
}Shared_data;

int main(void){

	sem_t *writer, *readers, *sem;
	Shared_data *shared_data;
	int fd, size = sizeof(Shared_data), rd, wr;
	char print[100];

	fd = shm_open("/sharedm", O_CREAT|O_EXCL|O_RDWR, S_IRUSR|S_IWUSR);
	ftruncate(fd, size);
	shared_data = mmap(NULL, size, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);

	if ((writer = sem_open("writer", O_CREAT, 0644, 0)) == SEM_FAILED) {
		perror("No sem_open()");
		exit(1);
	}

	if ((readers = sem_open("readers", O_CREAT, 0644, 0)) == SEM_FAILED) {
		perror("No sem_open()");
		exit(1);
	}

	if ((sem = sem_open("sem", O_CREAT, 0644, 1)) == SEM_FAILED) {
		perror("No sem_open()");
		exit(1);
	}

	sem_wait(sem);
	time_t rawtime;
  	struct tm * timeinfo;
  	time ( &rawtime );
  	timeinfo = localtime ( &rawtime );
	sprintf(shared_data -> string, "PID: %d\n Current time: %s\n", getpid(), asctime(timeinfo));
	sem_post(writer);
	sem_getvalue(readers, &rd);
	sem_getvalue(writer, &wr);
	sprintf(print, "Number of writers: %d\n Number of readers: %d\n", wr, rd);
	write(1, print, strlen(print));
	sem_post(sem);

	return 0;

}

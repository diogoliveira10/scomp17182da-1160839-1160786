#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <semaphore.h>

typedef struct{
	char string[100];
}Shared_data;

int main(void){

	sem_t *writer, *readers;
	Shared_data *shared_data;
	int fd, size = sizeof(Shared_data), rd;
	char print[100];

	fd = shm_open("/sharedm", O_EXCL|O_RDWR, S_IRUSR|S_IWUSR);
	ftruncate(fd, size);
	shared_data = mmap(NULL, size, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);

	if ((writer = sem_open("writer", O_CREAT, 0644, 0)) == SEM_FAILED) {
		perror("No sem_open()");
		exit(1);
	}

	if ((readers = sem_open("readers", O_CREAT, 0644, 0)) == SEM_FAILED) {
		perror("No sem_open()");
		exit(1);
	}

	sem_wait(writer);
	sem_post(readers);
	sem_getvalue(readers, &rd);
	sprintf("%s\nNumber of readers: %d", shared_data -> string, rd);
	write(1, print, strlen(print));
	sem_wait(readers);

	return 0;
}

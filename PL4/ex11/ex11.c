#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <semaphore.h>

int main(void){

	pid_t p;
	sem_t *passengers, *in_out;
	int i, status;
	time_t t;

	if ((passengers = sem_open("passengers", O_CREAT|O_EXCL, 0644, 200)) == SEM_FAILED) {
		perror("No sem_open()");
		exit(1);
	}

	if ((in_out = sem_open("in_out", O_CREAT|O_EXCL, 0644, 3)) == SEM_FAILED) {
		perror("No sem_open()");
		exit(1);
	}

	for (i = 0; i < 300; i++){
		p = fork();
		if (p == -1){
			perror("Fork falhou\n");
			exit(0);
		} else if (p == 0){
			srand((unsigned) time(&t));
			sleep((rand() % 5));
			sem_wait(passengers);
			sem_wait(in_out);
			sem_post(in_out);
			srand((unsigned) time(&t));
			sleep((rand() % 5));
			sem_wait(in_out);
			sem_post(passengers);
			sem_post(in_out);
			exit(0);
		}
	}

	while(wait(&status) > 0);

	sem_unlink("in_out");
	sem_unlink("passengers");

	return 0;
}
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <semaphore.h>

typedef struct{
	char line[50][80];
	int lines;
} Shared_data;

int main(void){
		
	sem_t *sem;
	int fd, size = sizeof(Shared_data);
	Shared_data *shared_data;
	
	fd = shm_open("/sharedm", O_CREAT|O_RDWR, S_IRUSR|S_IWUSR);
	ftruncate(fd, size);
	shared_data = mmap(NULL, size, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);

	if ((sem = sem_open("semaforo", O_CREAT, 0644, 1)) == SEM_FAILED) {
		perror("No sem_open()");
		exit(1);
	}
	
	sem_wait(sem);
	sprintf(shared_data -> line[shared_data -> lines], "I'm the father - with PID %d", getpid());
	printf("%s\n", shared_data -> line[shared_data -> lines]);
	shared_data -> lines++;
	sleep(5);
	sem_post(sem);
	
	close(fd);
	munmap(shared_data, size);
	
	return 0;
	
}

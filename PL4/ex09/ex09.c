#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <semaphore.h>


void buy_chips(){
	write(1, "Chips\n", strlen("Chips\n"));
	return;
}

void buy_beer(){
	write(1, "Beer\n", strlen("Beer\n"));
	return;
}

void eat_and_drink(){
	write(1, "Eat and Drink\n", strlen("Eat and Drink\n"));
	return;
}

int main(void){

	pid_t p;
	int i, valor, status;
	sem_t *barreira, *sem, *cont;

	if ((barreira = sem_open("barreirav3", O_CREAT|O_EXCL, 0644, 0)) == SEM_FAILED) {
		perror("No sem_open()");
		exit(1);
	}

	if ((sem = sem_open("semv3", O_CREAT|O_EXCL, 0644, 1)) == SEM_FAILED) {
		perror("No sem_open()");
		exit(1);
	}

	if ((cont = sem_open("contadorv3", O_CREAT|O_EXCL, 0644, 0)) == SEM_FAILED) {
		perror("No sem_open()");
		exit(1);
	}

	for (i = 0; i < 2; i++){
		p = fork();
		if (p == -1){
			perror("Fork falhou\n");
			exit(-1);
		} else if (p == 0){
			if (i == 0){
				sem_wait(sem);
				buy_beer();
				sem_post(cont);
				sem_post(sem);
			} else{
				sem_wait(sem);
				buy_chips();
				sem_post(cont);
				sem_post(sem);
			}
			sem_getvalue(cont, &valor);
			if (valor == 2) sem_post(barreira);
			sem_wait(barreira);
			eat_and_drink();
			sem_post(barreira);
			exit(0);
		}
	}

	while(wait(&status) > 0);

	sem_unlink("semv3");
	sem_unlink("barreirav3");
	sem_unlink("contadorv3");

	return 0;
}

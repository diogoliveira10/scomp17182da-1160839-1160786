#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <semaphore.h>

typedef struct{
	int number;
	char name[100];
	char address[100];
} User;

typedef struct{
	User records[100];
	int next;
} Shared_data;

int main(void){

	int fd, size = sizeof(Shared_data), i, number;
	Shared_data *shared_data;
	sem_t *sem;
	char info[200];

	fd = shm_open("/sharedm", O_CREAT|O_RDWR, S_IRUSR|S_IWUSR);
	ftruncate(fd, size);
	shared_data = mmap(NULL, size, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);

	if ((sem = sem_open("consult", O_CREAT, 0644, 1)) == SEM_FAILED) {
		perror("No sem_open()");
		exit(1);
	}

	sem_wait(sem);
	write(1, "Numero de identificacao:\n", strlen("Numero de identificacao:\n"));
	scanf("%d", &number);
	for (i = 0; i < shared_data -> next; i++){
		if (shared_data -> records[i].number == number){
			sprintf(info, "Numero: %d\nNome: %s\nEndereco: %s\n", shared_data -> records[i].number, shared_data -> records[i].name, shared_data -> records[i].address);
			write(1, info, strlen(info));
		} else if (i == shared_data -> next - 1) write(1, "Utilizador nao encontrado\n", strlen("Utilizador nao encontrado\n")); 
	}

	sem_post(sem);

	close(fd);
	munmap(shared_data, size);


	return 0;
}

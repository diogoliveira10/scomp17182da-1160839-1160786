#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <semaphore.h>

typedef struct{
	int number;
	char name[100];
	char address[100];
} User;

typedef struct{
	User records[100];
	int next;
} Shared_data;

int main(void){
	int fd, size = sizeof(Shared_data);
	Shared_data *shared_data;
	sem_t *sem;
	User user;

	fd = shm_open("/sharedm", O_CREAT|O_RDWR, S_IRUSR|S_IWUSR);
	ftruncate(fd, size);
	shared_data = mmap(NULL, size, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);

	if ((sem = sem_open("insert", O_CREAT, 0644, 1)) == SEM_FAILED) {
		perror("No sem_open()");
		exit(1);
	}

	sem_wait(sem);
	write(1, "Nome:\n", strlen("Nome:\n"));
	scanf("%s", user.name);
	write(1, "Numero:\n", strlen("Numero:\n"));
	scanf("%d", &user.number);
	write(1, "Endereco:\n", strlen("Endereco:\n"));
	scanf("%s", user.address);
	if (shared_data -> next == 100) write(1, "No more space\n", strlen("No more space\n"));
	shared_data -> records[shared_data -> next] = user;
	shared_data -> next++;
	sem_post(sem);

	close(fd);
	munmap(shared_data, size);

	return 0;

}


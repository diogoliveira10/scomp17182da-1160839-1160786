#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <semaphore.h>

int main(void){
	
	int i, j, status, number[1600], out, lineNumber = 0;
	sem_t *sem_n, *sem_out;
	pid_t p;
	FILE *f_n, *f_out;
	
	if ((sem_n = sem_open("numbers", O_CREAT|O_EXCL, 0644, 1)) == SEM_FAILED) {
		perror("No sem_open()");
		exit(1);
	}

	if ((sem_out = sem_open("output", O_CREAT|O_EXCL, 0644, 1)) == SEM_FAILED) {
		perror("No sem_open()");
		exit(1);
	}
	
	for(i = 0; i < 8; i++){
		p = fork();
		if (p == -1){
			perror("Fork falhou\n");
			exit(-1);
		} else if (p == 0){
			sem_wait(sem_n);
			f_n = fopen("numbers.txt", "r");
			for(j = i*200; j < (i+1)*200; j++){
				while(lineNumber != i*200){
					fscanf(f_n, "%d", &number[lineNumber]);
					lineNumber++;
				}
				fscanf(f_n, "%d", &number[j]);
			}
			fclose(f_n);
			sem_post(sem_n);
			sem_wait(sem_out);
			f_out = fopen("output.txt", "a");
			for(j = i*200; j < (i+1)*200; j++){
				fprintf(f_out, "%d\n", number[j]);
			}
			fclose(f_out);
			sem_post(sem_out);
			exit(0);
		}
	}
	
	while(wait(&status) > 0);
	
	f_out = fopen("output.txt", "r");
	while (fscanf(f_out, "%d", &out) != EOF){
		printf("%d\n", out);
	}
	fclose(f_out);
	
	sem_unlink("numbers");

	sem_unlink("output");
	
	return 0;
	
}

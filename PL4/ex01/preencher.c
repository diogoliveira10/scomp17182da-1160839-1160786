#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <semaphore.h>

int main(void){
	
	FILE *f;
	int i;
	
	f = fopen("numbers.txt", "w");
	for (i = 0; i < 1600; i++){
		fprintf(f, "%d", i);
		fprintf(f, "\n");
	}
	fclose(f);
	
	return 0;
}


#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <semaphore.h>

int main(void){

	sem_t *sem[3];
	char name[12], print[100];
	pid_t p;
	int i, status;

	for (i = 0; i < 3; i++){
		sprintf(name, "sem%d", i);
		if ((sem[i] = sem_open(name, O_CREAT|O_EXCL, 0644, 0)) == SEM_FAILED) {
			perror("No sem_open()");
			exit(1);
		}
	}

	for (i = 0; i < 3; i++){
		p = fork();
		if (p == -1){
			perror("Fork falhou\n");
			exit(-1);
		} else if (p == 0){
			if (i == 0){
				write(1, "Sistemas ", strlen("Sistemas "));
				sem_post(sem[0]);
				sem_wait(sem[2]);
				write(1, "a ", strlen("a "));
				sem_post(sem[0]);
				exit(0);
			} else if (i == 1){
				sem_wait(sem[0]);
				write(1, "de ", strlen("de "));
				sem_post(sem[1]);
				sem_wait(sem[0]);
				write(1, "melhor ", strlen("melhor "));
				sem_post(sem[1]);
				exit(0);
			} else{
				sem_wait(sem[1]);
				write(1, "Computadores -", strlen("Computadores -"));
				sem_post(sem[2]);
				sem_wait(sem[1]);
				write(1, "disciplina!\n", strlen("disciplina!\n"));
				exit(0);
			}
		}
	}

	while(wait(&status) > 0);

	for (i = 0; i < 3; i++){
		sprintf(name, "sem%d", i);
		sem_unlink(name);
	}

	return 0;
}

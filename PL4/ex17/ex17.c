#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <semaphore.h>

void priority(sem_t *cont, sem_t *vip, sem_t *special, sem_t *normal, int valor){
	sem_getvalue(cont, &valor);
	if(valor == 300){
		sem_post(vip);
		sem_wait(cont);
		sem_getvalue(cont, &valor);
		if(valor == 300){
			sem_post(special);
			sem_wait(cont);
			sem_getvalue(cont, &valor);
			if(valor == 300){
				sem_post(normal);
				sem_wait(cont);
			}
		}
	}
}

int main(void){

	pid_t p;
	sem_t *full, *cont;
	int cont, valor, status;

	if ((cont = sem_open("contador", O_CREAT|O_EXCL, 0644, 0)) == SEM_FAILED) {
		perror("No sem_open()");
		exit(1);
	}

	if ((vip = sem_open("vip", O_CREAT|O_EXCL, 0644, 0)) == SEM_FAILED) {
		perror("No sem_open()");
		exit(1);
	}

	if ((special = sem_open("special", O_CREAT|O_EXCL, 0644, 0)) == SEM_FAILED) {
		perror("No sem_open()");
		exit(1);
	}

	if ((normal = sem_open("normal", O_CREAT|O_EXCL, 0644, 0)) == SEM_FAILED) {
		perror("No sem_open()");
		exit(1);
	}

	for(i = 0; i < 500; i++){
		p = fork();
		if(p == -1){
			perror("Fork falhou\n");
			exit(-1);
		} else if (p == 0){
			sem_getvalue(cont, &valor);
			if (valor == 300){
				if(i < 100){ 
					sem_wait(vip);
					sem_post(cont);
					sleep(5);
					priority(cont, vip, special, normal, valor);
					exit(0);
				} else if(i >= 100 && i < 250){
					sem_wait(special);
					sem_post(cont);
					sleep(5);
					priority(cont, vip, special, normal, valor);
					exit(0);
				} else{
					sem_wait(normal);
					sem_post(cont);
					sleep(5);
					priority(cont, vip, special, normal, valor);
					exit(0);
				}
			} else {
				sem_post(cont);
				sleep(5);
				priority(cont, vip, special, normal, valor);
			exit(0);
		}
	}

	while(wait(&status) > 0);

	sem_unlink("contador");
	sem_unlink("vip");
	sem_unlink("special");
	sem_unlink("normal");

	return 0;
}
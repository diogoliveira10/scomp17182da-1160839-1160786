#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <semaphore.h>

int main(void){

	sem_t *sem;
	pid_t p;
	int status;

	if ((sem = sem_open("semaforo", O_CREAT|O_EXCL, 0644, 0)) == SEM_FAILED) {
		perror("No sem_open()");
		exit(1);
	}


	p = fork();

	if (p == -1){
		perror("Fork failed\n");
		exit (-1);
	} else if (p == 0){
		printf("I'm the child\n");
		sem_post(sem);
		exit(1);
	} else{
		sem_wait(sem);
		printf("I'm the father\n");
	}

	wait(&status);

	sem_unlink("semaforo");


	return 0;
}

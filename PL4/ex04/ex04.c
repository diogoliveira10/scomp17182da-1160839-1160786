#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <semaphore.h>

typedef struct{
	char line[50][80];
	int lines;
} Shared_data;

int main(void){
		
	sem_t *sem;
	int fd, size = sizeof(Shared_data);
	Shared_data *shared_data;
	struct timespec ts;
	
	fd = shm_open("/shm", O_CREAT|O_RDWR, S_IRUSR|S_IWUSR);
	ftruncate(fd, size);
	shared_data = mmap(NULL, size, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);

	if ((sem = sem_open("sem", O_CREAT, 0644, 1)) == SEM_FAILED) {
		perror("No sem_open()");
		exit(1);
	}

	ts.tv_sec += 12;
	
	if (sem_timedwait(sem, &ts) == -1 && errno == ETIMEDOUT){
		perror("Timed out");
		exit(0);
	}
	//free?
	sprintf(shared_data -> line[shared_data -> lines], "I'm the father - with PID %d", getpid());
	shared_data -> lines++;
	sleep(5);
	sem_post(sem);
	
	close(fd);
	munmap(shared_data, size);
	
	return 0;
	
}
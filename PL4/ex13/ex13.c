#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <semaphore.h>
#include "createshm.h"
#include "unlinkshm.h"

typedef struct{
	int buffer[10];
	int next;
} Shared_data;

int main(void){

	int size = sizeof(Shared_data), i, exchange = 0, next, status;
	Shared_data *shared_data;
	pid_t p;
	sem_t *new_data, *full, *sem;

	if ((new_data = sem_open("new_datav12", O_CREAT|O_EXCL, 0644, 0)) == SEM_FAILED) {
		perror("No sem_open()");
		exit(1);
	}

	if ((full = sem_open("fullv12", O_CREAT|O_EXCL, 0644, 0)) == SEM_FAILED) {
		perror("No sem_open()");
		exit(1);
	}

	if ((sem = sem_open("semv12", O_CREAT|O_EXCL, 0644, 1)) == SEM_FAILED) {
		perror("No sem_open()");
		exit(1);
	}

	shared_data = (Shared_data *) create_shm(size);

	for (i = 0; i < 2; i++){
		p = fork();
		if (p == -1){
			perror("Fork falhou\n");
			exit (0);
		} else if (p == 0){
			while(exchange < 30){
				if (shared_data -> next == 10){
					sem_post(new_data);
					sem_wait(full);
				}
				sem_wait(sem);
				shared_data -> buffer[shared_data -> next] = exchange;
				exchange++;
				shared_data -> next++;
				sem_post(sem);
			}
			exit(0);
		}
	}

	while(exchange < 3){
		sem_wait(new_data);
		for (i = 0; i < 10; i++){
			write(1, &(shared_data -> buffer[i]), sizeof(int));
		}
		exchange++;
		shared_data -> next = 0;
		sem_post(full);
	}

	while(wait(&status) > 0);

	sem_unlink("semv12");
	sem_unlink("fullv12");
	sem_unlink("new_datav12");

	unlinkshm(shared_data, size);

	return 0;
}
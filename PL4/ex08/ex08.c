#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <semaphore.h>
#include "createshm.h"
#include "unlinkshm.h"

typedef struct{
	int s;
	int c;
	int strSize;
} Shared_data;

int main(void){

	pid_t p;
	int i, status, size = sizeof(Shared_data);
	Shared_data *shared_data;
	sem_t *sem;

	shared_data = (Shared_data *) create_shm(size);

	if ((sem = sem_open("sem", O_CREAT|O_EXCL, 0644, 1)) == SEM_FAILED) {
		perror("No sem_open()");
		exit(1);
	}

	for (i = 0; i < 2; i++){
		p = fork();
		if (p == -1){
			perror("Fork falhou\n");
			exit(-1);
		} else if (p == 0){
			if (i == 0){
				while(shared_data -> strSize < 20){
					sem_wait(sem);
					if (shared_data -> s < 2){
						write(1, "S", strlen("S"));
						shared_data -> s++;
						shared_data -> c = 0;
						shared_data -> strSize++;
					}
					sem_post(sem);
				}
				exit(0);
			} else{
				while(shared_data -> strSize < 20){
					sem_wait(sem);
					if (shared_data -> c < 2){
						write(1, "C", strlen("C"));
						shared_data -> c++;
						shared_data -> s = 0;
						shared_data -> strSize++;
					}
					sem_post(sem);
				}
				exit(0);
			}
		}
	}

	while(wait(&status) > 0);

	sem_unlink("sem");

	unlinkshm(shared_data, size);

	return 0;
}

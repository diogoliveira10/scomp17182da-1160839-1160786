#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <semaphore.h>

int main(void){

	pid_t p;
	int status;

	if ((east = sem_open("east", O_CREAT|O_EXCL, 0644, 1)) == SEM_FAILED) {
		perror("No sem_open()");
		exit(1);
	}

	if ((west = sem_open("west", O_CREAT|O_EXCL, 0644, 1)) == SEM_FAILED) {
		perror("No sem_open()");
		exit(1);
	}

	for(i = 0; i < 2; i++){
		p = fork();
		if (p == -1){
			perror("Fork falhou\n");
			exit(-1);
		} else if (p == 0){
			if(i%2 == 0){
				sem_wait(east);
				sleep(30);
				sem_post(west);
			} else{
				sem_wait(west);
				sleep(30);
				sem_post(east);
			}
		}
	}

	while(wait(&status) > 0);

	sem_unlink("east");
	sem_unlink("west");

	return 0;
}
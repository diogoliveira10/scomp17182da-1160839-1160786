#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <semaphore.h>
#include "createshm.h"
#include "unlinkshm.h"

typedef struct{
	int ticket;
} Shared_data;

int main(void){

	int size = sizeof(Shared_data), next = 0, i, status;
	sem_t *client, *served;
	pid_t p;
	Shared_data *shared_data;

	shared_data = (Shared_data *) create_shm(size);

	if ((client = sem_open("clientv3", O_CREAT|O_EXCL, 0644, 0)) == SEM_FAILED) {
		perror("No sem_open()");
		exit(1);
	}

	if ((served = sem_open("servedv3", O_CREAT|O_EXCL, 0644, 0)) == SEM_FAILED) {
		perror("No sem_open()");
		exit(1);
	}

	for (i = 0; i < 10; i++){
		p = fork();
		if (p == -1){
			perror("Fork falhou\n");
			exit(0);
		} else if (p == 0){
			execlp("./client", "./client", NULL);
			perror("Exec falhou\n");
			exit(-1);
		}
	}

	while (i > -1){
		sem_wait(client);
		shared_data -> ticket = next;
		next++;
		sem_post(served);
	}

	while(wait(&status) > 0);

	sem_unlink("clientv3");
	sem_unlink("servedv3");

	unlinkshm(shared_data, size);

	return 0;
}
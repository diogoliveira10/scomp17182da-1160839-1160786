#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <semaphore.h>

typedef struct{
	int ticket;
} Shared_data;

int main(void){

	Shared_data *shared_data;
	sem_t *client, *sem, *served;
	int fd, size = sizeof(Shared_data);
	char my_ticket[100];

	fd = shm_open("/sharedm", O_EXCL|O_RDWR, S_IRUSR|S_IWUSR);
	ftruncate(fd, size);
	shared_data = mmap(NULL, size, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);

	if ((client = sem_open("clientv3", O_CREAT, 0644, 0)) == SEM_FAILED) {
		perror("No sem_open()");
		exit(1);
	}

	if ((sem = sem_open("sem", O_CREAT, 0644, 1)) == SEM_FAILED) {
		perror("No sem_open()");
		exit(1);
	}

	if ((served = sem_open("servedv3", O_CREAT, 0644, 0)) == SEM_FAILED) {
		perror("No sem_open()");
		exit(1);
	}

	sem_wait(sem);
	sem_post(client);
	sem_wait(served);
	sprintf(my_ticket, "%d ticket is : %d\n", getpid(), shared_data -> ticket);
	write(1, my_ticket, strlen(my_ticket));
	sem_post(sem);

	close(fd);
	munmap(shared_data, size);

	exit(0);

	return 0;
}
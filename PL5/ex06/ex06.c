#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <semaphore.h>
#include <pthread.h>

int number;

void* thread_func(void *arg){

	int i, j, flag;
	
	for (i = 2; i <= number; i++){
		flag = 1;
		for (j = 2; j <= i/2; j++) {
			if (i % j == 0){
				flag = 0; 
				break;
			}
		}
		if(flag) printf("%d\n", i);
	}

	pthread_exit(NULL);

}

int main(void){

	pthread_t thread;

	printf("Number: \n");
	scanf("%d", &number);

	pthread_create(&thread, NULL, thread_func, NULL);
	pthread_join(thread, NULL);

	return 0;
}

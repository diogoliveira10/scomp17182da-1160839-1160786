#include <pthread.h>
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>

#define SIZE 50

typedef struct{
	int number;
	char *name;
	char *address;
}Str;

pthread_t threads[5];
Str structs[5];

void* thread_func(void *arg){
	int ID = *((int*)arg);
	
	Str *s = &structs[ID];
	
	if(ID != 0){
		pthread_join(threads[ID-1],NULL);
	}
	printf("\n Elemento do Array: \n Number: %d \n Name: %s \n Address: %s \n", s->number, s->name, s->address),
	
	pthread_exit((void*)NULL);
}

int main(){
	int i;
	int arrayAux[5];
	
	for (i = 0; i < 5; i++){
		//numero
		structs[i].number = i;
		//nome (malloc reserva espaço para a string)
		char *temp = (char*)malloc(SIZE);
		sprintf(temp, "nome-%d", i);
		structs[i].name = temp;
		//address
		char *temp2 = (char*)malloc(SIZE);
		sprintf(temp2, "address-%d", i);
		structs[i].address = temp2;
	}
	

	
	for (i = 0; i < 5; i++){
		arrayAux[i] = i;
		pthread_create(&threads[i], NULL, thread_func, (void*) &arrayAux[i]);
	}
	
	pthread_join(threads[4], NULL); 
	
	
	return 0;

}




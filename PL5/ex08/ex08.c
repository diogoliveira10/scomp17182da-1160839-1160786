#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <semaphore.h>
#include <pthread.h>

#define SIZE 1000

int result[SIZE], data[SIZE], aux_cond = 0;
pthread_mutex_t mux;
pthread_cond_t cond[4]; 

void* thread_func(void *arg){
	int i, pos = *((int *)arg);

    for (i = pos * (SIZE / 5); i < (pos + 1) * (SIZE / 5); i++){
    	result[i] = data[i] * 2 + 10;
    }

    pthread_mutex_lock(&mux);
    
    while(aux_cond != pos){
		 pthread_cond_wait(&cond[pos-1], &mux);
	}
	
    for (i = pos * (SIZE / 5); i < (pos + 1) * (SIZE / 5); i++){
    	printf("Position %d: %d\n", i, result[i]);
    }
    
    aux_cond++;
    pthread_cond_signal(&cond[pos]);
    pthread_mutex_unlock(&mux);
	
	pthread_exit(NULL);
}

int main(){

	pthread_t threads[5];
	int i;
	int args[5];

	for(i = 0; i < SIZE; i++){
		data[i] = rand() % 10;
	}

	pthread_mutex_init(&mux, NULL);

	for (i = 0; i < 4; i++){
		pthread_cond_init(&cond[i], NULL);
	}

	for(i = 0; i < 5; i++) {
		args[i] = i; 
        pthread_create(&threads[i], NULL, thread_func, (void *) &args[i]);       
    }

    for(i = 0; i < 5; i++) {
		pthread_join(threads[i], NULL);   
	}

	pthread_mutex_destroy(&mux);
	
	for (i = 0; i < 4; i++){
		pthread_cond_destroy(&cond[i]);
	}

	return 0;
}

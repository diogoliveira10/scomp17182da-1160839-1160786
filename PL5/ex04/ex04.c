#include <pthread.h>
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>

#define SIZE 5
#define SIZE_FILL 2
/*
int m1[SIZE][SIZE];
int m2[SIZE][SIZE];*/
int array[2][SIZE][SIZE];
int result[SIZE][SIZE];

void* thread_func_fill(void *arg){
	int n_matriz = (*((int *) arg));
	srand(time(NULL) * n_matriz);
	
	int i, j, num;
	for (i = 0; i < SIZE; i++){
		for (j = 0; j < SIZE; j++){
			num = rand() % 6;
			printf("Num: %d | ", num);
			array[n_matriz][i][j] = num;
		}
		printf("\n");
	}
	
	printf("\n");
	pthread_exit((void*)NULL);
}

void* thread_func_multi(void *arg){
	int n_line = *((int*)arg);
	
	int sum =0,j,k;
	
	for (j = 0; j < SIZE; j++){
		for (k = 0; k < SIZE ; k++){
			sum += (array[0][n_line][k] * array[1][k][j]);
		}
		result[n_line][j] = sum;
		sum = 0;
	}
	
	pthread_exit((void*)NULL);
}

int main(){
	//fill
	pthread_t threads[SIZE_FILL];

	int i,j,k;
	int arrayAux[SIZE];
	int arrayAux2[SIZE_FILL];
	
	for (i = 0; i < SIZE_FILL; i++){
		arrayAux2[i] = i;
		pthread_create(&threads[i], NULL, thread_func_fill, &arrayAux2[i]);
	}
	
	for(i = 0 ;i < SIZE_FILL; i++){
		pthread_join(threads[i], NULL);
	}
	
	
	//multiply
	pthread_t threads_multi[SIZE];
	
	for (i = 0; i < SIZE; i++){
		arrayAux[i] = i;
		pthread_create(&threads_multi[i], NULL, thread_func_multi, (void*) &arrayAux[i]);
	}
	
	for(i = 0 ;i < SIZE; i++){
		pthread_join(threads_multi[i], NULL);
	}
	
	//matrizes
	for (k = 0; k < 2 ; k++){
		printf("\n");
		for(i = 0; i < SIZE; i++){
			for(j = 0; j < SIZE; j++){
				printf("%d ",array[k][i][j]);
			}
			printf("\n");
		}
		printf("\n");
	}
	
	//resultado
	printf("\n Result: \n");
	for(i = 0; i < SIZE; i++){
		for(j = 0; j < SIZE; j++){
			printf("%d ",result[i][j]);
		}
		printf("\n");
	}
	
	return 0;
}
	
